TrelloClone.Views.BoardShowView = Backbone.CompositeView.extend({
  template: JST['boards/show'],

  events: {
    "click .delete-board": "deleteBoard"
  },

  initialize: function () {
    board = this.model;
    this.listenTo(this.model, 'sync', this.render);
    //this.listenTo(this.collection, 'add', this.addList);
  },

  render: function () {
    var content = this.template({ board: this.model });
    this.$el.html(content);
    this.removeAllLists();
    //remember this
    this.collection.each(this.addList.bind(this));
    this.attachSubviews();
    return this;
  },

  addList: function (list) {
    var listView = new TrelloClone.Views.ListShowView({
      model: list,
      collection: this.collection
    });
    this.addSubview(".lists", listView);
  },

  addCreateList: function () {
    var createListForm = new TrelloClone.Views.NewList({board: this.model});
    this.addSubview(".new-list", createListForm);
  },

  removeAllLists: function () {
    var subviews = this.subviews('.lists');
    var views = subviews.clone();
    views.forEach(this.removeSubview.bind(this, '.lists'));

  },

  deleteBoard: function (event) {
    event.preventDefault();
    this.model.destroy({
      success: function () {
        this.collection.remove(this.model);
        Backbone.history.navigate("#", { trigger:true });
      }.bind(this)
    });
  }
})
