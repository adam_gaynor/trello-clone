TrelloClone.Views.ListShowView = Backbone.CompositeView.extend({
  template: JST['lists/show_list'],

  render: function () {
    var content = this.template({ list: this.model });
    this.$el.html(content);
    this.attachSubviews();
    return this;
  }
})
