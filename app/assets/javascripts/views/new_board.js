TrelloClone.Views.NewBoard = Backbone.View.extend({
  template: JST['boards/new_board'],

  events: {
    "submit .new-board": "createBoard"
  },

  initialize: function () {
    if (!this.collection) {
      this.collection = TrelloClone.Collections.boards;
    }
  },

  render: function () {
    var content = this.template({ board: this.model });
    this.$el.html(content);
    return this;
  },

  createBoard: function () {
    event.preventDefault();

    var boardData = $(event.target).serializeJSON();
    console.log(boardData);
    this.model.save(boardData, {
      success: function () {
        this.collection.add(this.model);
        Backbone.history.navigate("#api/boards/" + this.model.id, { trigger: true });
      }.bind(this)
    });
  }

})
