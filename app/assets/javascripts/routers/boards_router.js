TrelloClone.Routers.BoardsRouter = Backbone.Router.extend({

  routes: {
    '': 'boardsIndex',
    'api/boards/new': 'createBoard',
    'api/boards/:id': 'showBoard',
    'api/boards/:board_id/lists': 'createList'
  },

  initialize: function (options) {
    this.boards = options.boards;
    this.$rootEl = options.$rootEl;
  },

  boardsIndex: function () {
    var indexView = new TrelloClone.Views.BoardsIndex({
      collection: this.boards
    });
    this._swapView(indexView);
  },

  createBoard: function (event) {
    var board = new TrelloClone.Models.Board();
    var newBoardView = new TrelloClone.Views.NewBoard({
      model: board,
      collection: this.boards
    });
    this._swapView(newBoardView);
  },

  showBoard: function (id) {
    var board = this.boards.getOrFetch(id);
    var boardView = new TrelloClone.Views.BoardShowView({
      model: board,
      collection: board.lists()
    })
    this._swapView(boardView);
  },

  createList: function (board_id) {
    var list = new TrelloClone.Models.List();
    var listView = new TrelloClone.Views.ListShowView({
      model: list,
      collection: boards.get(board_id).lists()
    });
    //this._swapView(listView);
  },

  _swapView: function (newView) {
    this._currentView && this._currentView.remove();
    this._currentView = newView;
    this.$rootEl.html(newView.$el);
    newView.render();
  }
})
