TrelloClone.Collections.Lists = Backbone.Collection.extend({
  url: function () {
    return this.board.url() + '/lists';
  },

  comparator: "title",

  model: TrelloClone.Models.List,

  initialize: function (models, options) {
    this.board = options.board;
  }
})
