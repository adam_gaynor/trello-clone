TrelloClone.Models.List = Backbone.Model.extend({
  rootUrl: function () {
    return this.board.url() + "/entries";
  }
})
